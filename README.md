 
# Nostra App Test

Containerize Spring Boot Application with Docker, and docker-compose
_____
# Prerequisite
* Docker installed on the system
* Docker-Compose installed on the system
_____
## Docker and Docker-compose Instalation
Please follow this instruction if docker not yet avaiable on your system. you can skip this section if you already installed docker and docker-compose.

```
$ sudo yum check-update
$ curl -fsSL https://get.docker.com/ | sh
$ sudo systemctl start docker
$ sudo systemctl status docker
$ sudo systemctl enable docker
$ sudo usermod -aG docker $(whoami)
```

Now we'll continue to install docker-compose script

```
$ sudo curl -L "https://github.com/docker/compose/releases/download/1.23.2/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
$ sudo chmod +x /usr/local/bin/docker-compose
$ docker-compose --version
```
_____
# Instalation
* Clone this repository
* Navigate to folder we just clone and run docker-compose

```
$ cd Demo
$ docker-compose up -d --build
```

Note: you can check the logs with 'docker logs spring_test -f'

* Import the sql file to your database

```
$ docker exec -t postgres pg_dumpall -c -U nostra > dump_`date +%d-%m-%Y"_"%H_%M_%S`.sql   # For Backing up the database
$ cat <sql_dump_file>.sql | docker exec -i postgres psql -U nostra     # For Restore the database
```
_____
# Usage
The Application will exposed on port 8080 with 2 endpoints: "/api/v1/users" and "/api/v1/hello"

Note: you can test the api function using Curl command or external application like Insomnia/Postman