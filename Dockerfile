
FROM maven:3.6.3-jdk-11 as builder
RUN apt-get update && apt-get install git curl -y

WORKDIR /cloudwolf
RUN git clone --branch master --single-branch https://Accalina@bitbucket.org/Accalina/demo.git

WORKDIR /cloudwolf/demo
RUN mvn clean install spring-boot:repackage -e -X -D maven.test.skip=true

FROM openjdk:8-jdk-alpine as runtime
WORKDIR /cloudwolf
COPY --from=builder /cloudwolf/demo /cloudwolf
# CMD java -jar target/demo-0.0.1-SNAPSHOT.jar